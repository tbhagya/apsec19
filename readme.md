## Introduction
This study examines the appropriateness of Symbolic Machine Learning algorithms (i.e., the common decision tree algorithm, **J48** and two most popular schemes for rule learning, **RIPPER** and **PART**) to automatically form services' mock skeletons that semantically closely resemble the behaviour of actual services.

All the experiments have been done within the **Waikato Environment for Knowledge Analysis (WEKA)** environment, employing network traffic datasets extracted from a few different successful, large-scale HTTP services (i.e., **GitHub**, **Twitter**, **Google** and **Slack**). 

## Using Script

We provide access to the scripts used to generate classifers. Scripts can be accessed by cloning the repository. Using these scripts, you can build models, one for each response feature at a time, for a particular dataset for your own use.

### Checking Attributes in Datasets

**check.sh** script with options **GHTraffic**/**Twitter**/**Google**/**Slack** can be used to obtain attributes in each dataset. 

To retrieve the attribute set for **GoogleTaskLists** dataset, clone the repository into **script** folder and run the following command:

```
./check.sh -Google
```

It presents metadata on attributes such as index, name and number of distinct values. By referring to this list, you can identify the index of the attribute to use as the target and indexes you want to eliminate (that you do not require to influence model learning). 

These details can be used when adding content to indexes.txt file.

### Generating Classifier for Target Attribute

Update **indexes.txt** file located in **script** by adding the index of the target attribute and indexes of attributes to eliminate

**build.sh** script with options **GHTraffic**/**Twitter**/**Google**/**Slack** and **J48**/**JRip**/**PART** can be used to create either edition. 

To generate the **J48** classifer for the specified target attribute in **Google** dataset, run the following command:

```
./build.sh -Google -J48 
```

