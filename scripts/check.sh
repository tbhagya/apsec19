#!/bin/bash 
echo "Start checking dataset" 
mvn clean install
echo "Attributes in dataset" 
mvn -q clean compile exec:java -Dexec.mainClass="massey.ac.nz.weka.processor.CheckAttributesDetails" -Dexec.args="-$*"
echo "You can select target index & indexes to remove from above list"
exit 0