package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

import java.io.File;

/**
 * This class uses to generate arff file based on csv
 * to be used as input to weka
 * @author thilini bhagya
 */
public class CsvToArffConverter {
    public static void convertCsvDataFileToArffFormat(String fileName) throws Exception {
        // load CSV file
        CSVLoader loader = new CSVLoader();
        loader.setSource(new File(fileName+".csv"));
        Instances data = loader.getDataSet();

        // save data in ARFF format
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);
        saver.setFile(new File(fileName+".arff"));
        saver.writeBatch();
        // .arff file will be created in the output location
    }
}
