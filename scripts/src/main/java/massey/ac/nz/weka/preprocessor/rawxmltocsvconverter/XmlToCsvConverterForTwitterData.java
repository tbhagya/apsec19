package massey.ac.nz.weka.preprocessor.rawxmltocsvconverter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * This class extracts resource,time,method,status,url,request/response body and headers data of each xml record
 * in raw output-twitter.xml file
 * sorts data using resource identifiers
 * saves data in CSV format
 * @author thilini bhagya
 */
public class XmlToCsvConverterForTwitterData {
    public static void main(String argv[]) throws Exception
    {
        fileWriter(System.getProperty("user.dir") + File.separator + "outputTwitter.csv");
    }


    //read request.xml data
    private static void fileWriter(String fileName) throws Exception
    {
        FileWriter writer = new FileWriter(fileName);

        //csv file headers
        writer.append("resource");
        writer.append('|');
        writer.append("transaction");
        writer.append('|');
        writer.append("time");
        writer.append('|');
        writer.append("method");
        writer.append('|');
        writer.append("status");
        writer.append('|');
        writer.append("url");
        writer.append('|');
        writer.append("responseBody");
        writer.append('|');
        writer.append("requestHeaders");
        writer.append('|');
        writer.append("responseHeaders");
        writer.append('|');
        writer.append("requestBody");
        writer.append('\n');

        int i=0;

        File fXmlFile = new File(System.getProperty("user.dir") + File.separator + "output-twitter.xml");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        NodeList nList = doc.getElementsByTagName("httpSample");
        for (int temp = 0; temp < nList.getLength(); temp++)
        {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE)
            {
                Element eElement = (Element) nNode;

                //status
                String string = eElement.getElementsByTagName("responseHeader").item(0).getTextContent();
                String[] a = string.split(" ");

                //resource
                if((eElement.getElementsByTagName("method").item(0).getTextContent().matches("POST"))  ) {
                    if(a[1].matches("200")) {
                        String content = eElement.getElementsByTagName("responseData").item(0).getTextContent();
                        String c = content.substring(52, 71);
                        writer.append(c);
                        writer.append('|');
                        writer.append(String.valueOf(i++));
                        writer.append('|');
                        //time
                        writer.append(eElement.getAttribute("ts"));
                        writer.append('|');
                        //method
                        writer.append(eElement.getElementsByTagName("method").item(0).getTextContent());
                        writer.append('|');
                        //status
                        writer.append(a[1]);
                        writer.append('|');
                        //url
                        String url = eElement.getElementsByTagName("java.net.URL").item(0).getTextContent()+"?"+eElement.getElementsByTagName("queryString").item(0).getTextContent();
                        writer.append(url);
                        writer.append('|');
                        //response body
                        writer.append(content);
                        //request headers
                        writer.append('|');
                        String requestHeader = eElement.getElementsByTagName("requestHeader").item(0).getTextContent();
                        requestHeader = requestHeader.replaceAll("\n", "\t").replaceAll(": ","~");
                        if (requestHeader.endsWith("\t"))
                        {
                            requestHeader = requestHeader.substring(0, requestHeader.length() - 1);
                            writer.append(requestHeader);
                        }
                        //response header
                        writer.append('|');
                        String responseHeader = eElement.getElementsByTagName("responseHeader").item(0).getTextContent();
                        responseHeader = responseHeader.replaceAll("\n", "\t").replaceAll(": ","~");

                        if (responseHeader.endsWith("\t")) {
                            responseHeader = responseHeader.substring(0, responseHeader.length() - 1);
                            writer.append(responseHeader);


                        }
                        //new line
                        writer.append('\n');
                    }


                    else if (a[1].matches("404")){
                        String content = eElement.getElementsByTagName("responseData").item(0).getTextContent();
                        String url = eElement.getElementsByTagName("java.net.URL").item(0).getTextContent();
                        String id = url.substring(url.indexOf("destroy/")+8).substring(url.substring(url.indexOf("destroy/")+8).indexOf("1"),url.substring(url.indexOf("destroy/")+8).indexOf(".json"));
                        writer.append(id);
                        writer.append('|');
                        writer.append(String.valueOf(i++));
                        writer.append('|');
                        //time
                        writer.append(eElement.getAttribute("ts"));
                        writer.append('|');
                        //method
                        writer.append(eElement.getElementsByTagName("method").item(0).getTextContent());
                        writer.append('|');
                        //status
                        writer.append(a[1]);
                        writer.append('|');
                        //url
                        writer.append(url);
                        writer.append('|');
                        //response body
                        writer.append(content);
                        //request headers
                        writer.append('|');
                        String requestHeader = eElement.getElementsByTagName("requestHeader").item(0).getTextContent();
                        requestHeader = requestHeader.replaceAll("\n", "\t").replaceAll(": ","~");
                        if (requestHeader.endsWith("\t"))
                        {
                            requestHeader = requestHeader.substring(0, requestHeader.length() - 1);
                            writer.append(requestHeader);
                        }
                        //response header
                        writer.append('|');
                        String responseHeader = eElement.getElementsByTagName("responseHeader").item(0).getTextContent();
                        responseHeader = responseHeader.replaceAll("\n", "\t").replaceAll(": ","~");

                        if (responseHeader.endsWith("\t")) {
                            responseHeader = responseHeader.substring(0, responseHeader.length() - 1);
                            writer.append(responseHeader);


                        }
                        //new line
                        writer.append('\n');
                    }

                    else if (a[1].matches("403")){
                        //System.out.println(c);

                    }
                }

                else if ((eElement.getElementsByTagName("method").item(0).getTextContent().matches("GET")) && (a[1].matches("200")) ||  (a[1].matches("404"))){
                    String content = eElement.getElementsByTagName("responseData").item(0).getTextContent();
                    String url = eElement.getElementsByTagName("java.net.URL").item(0).getTextContent();
                    String id = url.substring(url.indexOf("=")+1);
                    writer.append(id);
                    writer.append('|');
                    writer.append(String.valueOf(i++));
                    writer.append('|');
                    //time
                    writer.append(eElement.getAttribute("ts"));
                    writer.append('|');
                    //method
                    writer.append(eElement.getElementsByTagName("method").item(0).getTextContent());
                    writer.append('|');
                    //status
                    writer.append(a[1]);
                    writer.append('|');
                    //action
                    writer.append(url);
                    writer.append('|');
                    //response body
                    writer.append(content);
                    //request headers
                    writer.append('|');
                    String requestHeader = eElement.getElementsByTagName("requestHeader").item(0).getTextContent();
                    requestHeader = requestHeader.replaceAll("\n", "\t").replaceAll(": ","~");
                    if (requestHeader.endsWith("\t"))
                    {
                        requestHeader = requestHeader.substring(0, requestHeader.length() - 1);
                        writer.append(requestHeader);
                    }
                    //response headers
                    writer.append('|');
                    String responseHeader = eElement.getElementsByTagName("responseHeader").item(0).getTextContent();
                    responseHeader = responseHeader.replaceAll("\n", "\t").replaceAll(": ","~");

                    if (responseHeader.endsWith("\t")) {
                        responseHeader = responseHeader.substring(0, responseHeader.length() - 1);
                        writer.append(responseHeader);


                    }
                    //new line
                    writer.append('\n');
                }
            }
        }
        writer.flush();
        writer.close();

    }

    //modify query string as a map
    public static Map<String, String> getQueryTokenMap(String a) throws URISyntaxException, UnsupportedEncodingException
    {
        Map<String, String> maps = new HashMap<String, String>();
        //decode query string
        String decoded = URLDecoder.decode(a, "UTF-8");
        StringTokenizer st = new StringTokenizer(decoded, ",=&");
        while (st.hasMoreElements())
        {
            String key = st.nextToken();
            String value = st.nextToken();
            maps.put(key, value);

        }
        return maps;
    }

}
