package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import massey.ac.nz.weka.Logging;

/**
 * main class to create csv file based on attribute-set selected in Twitter
 * and then to convert it to arff format which suitable for weka
 * @author thilini bhagya
 */

public class mainForSlack {
    static org.apache.log4j.Logger LOGGER = Logging.getLogger(mainForSlack.class);
    public static void main(String args[]) throws Exception {
        LOGGER.info("Generating a csv file relating with selected attributes");
        CsvFileGeneratorForSlackData.csvFileGeneratorWithAttributes("src/resources/slack-all","src/resources/outputSlack");
        LOGGER.info("Generating an arff file from the csv");
        CsvToArffConverter.convertCsvDataFileToArffFormat("src/resources/slack-all");


}}
