package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.*;

/**
 * This class generates csv file for Twitter dataset
 * related with attributes
 * @author thilini bhagya
 */
public class CsvFileGeneratorForTwitterData {

    static Set<String> responsebodykeylist = new TreeSet<String>();
    static Set<String> requestheaderskeylist = new TreeSet<String>();
    static Set<String> responseheaderskeylist = new TreeSet<String>();

    public static void csvFileGeneratorWithAttributes(String writeFileName, String readFileName) throws Exception {
        String csv = writeFileName + ".csv";
        CSVWriter writer = new CSVWriter(new FileWriter(csv), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        List<String[]> data = new ArrayList<String[]>();
        List<String> newCsvData = new ArrayList<String>();

        HttpTransaction.read(readFileName + ".csv");

        //extract key set for responseBody
        //extractKeySetForResponseBody();
        //extract key set for headers
        //extractKeySetForRequestHeaders();
        //extractKeySetForResponseHeaders();

        //add the header line
        data.add(new String[]{"method","requestHeader:Host","requestHeader:User-Agent",
                "responseHeader:cache-control","responseHeader:content-disposition","responseHeader:content-length",
                "responseHeader:content-type","responseHeader:date","responseHeader:expires","responseHeader:last-modified",
                "responseHeader:pragma","responseHeader:server","responseHeader:status",
                "responseHeader:strict-transport-security","responseHeader:x-connection-hash","responseHeader:x-content-type-options",
                "responseHeader:x-frame-options","responseHeader:x-rate-limit-limit","responseHeader:x-rate-limit-remaining",
                "responseHeader:x-rate-limit-reset","responseHeader:x-response-time","responseHeader:x-transaction",
                "responseHeader:x-twitter-response-tags","responseHeader:x-xss-protection",
                "statusCode",
                "json:contributors","json:coordinates","json:created_at",
                "json:entities.hashtags","json:entities.symbols", "json:entities.urls", "json:entities.user_mentions",
                "json:errors.code","json:errors.message",
                "json:favorite_count","json:favorited","json:geo","json:id",
                "json:id_str","json:in_reply_to_screen_name","json:in_reply_to_status_id","json:in_reply_to_status_id_str",
                "json:in_reply_to_user_id","json:in_reply_to_user_id_str","json:is_quote_status","json:lang","json:place",
                "json:retweet_count","json:retweeted","json:text","json:truncated",
                "json:user.contributors_enabled","json:user.created_at","json:user.default_profile",
                "json:user.default_profile_image","json:user.id","json:user.id_str","json:user.name",
                "json:user.screen_name","json:user.location","json:user.description","json:user.url",
                "json:user.favourites_count","json:user.follow_request_sent","json:user.followers_count",
                "json:user.following","json:user.friends_count","json:user.geo_enabled","json:user.has_extended_profile",
                "json:user.is_translation_enabled","json:user.is_translator","json:user.lang",
                "json:user.listed_count","json:user.notifications","json:user.profile_background_color",
                "json:user.profile_background_image_url","json:user.profile_background_image_url_https",
                "json:user.profile_background_tile","json:user.profile_banner_url","json:user.profile_image_url",
                "json:user.profile_image_url_https","json:user.profile_link_color","json:user.profile_sidebar_border_color",
                "json:user.profile_sidebar_fill_color","json:user.profile_text_color","json:user.profile_use_background_image",
                "json:user.protected","json:user.statuses_count","json:user.time_zone","json:user.translator_type",
                "json:user.utc_offset","json:user.verified",
                "hasAuthorizationToken",
                "uriSchema", "uriHost",
                "uriPathToken1", "uriPathToken2", "uriPathToken3", "uriPathToken4",
                "uriQueryToken1", "uriQueryToken2","uriQueryToken3",
                "uriFragmentToken1",
                "hasImmediatePreviousTransaction","immediatelyPreviousStatusCode", "immediatelyPreviousMethod",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToCreate", "hasURLInImmediatelyPreviousTransactionContainsATokenToRead",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate", "hasURLInImmediatelyPreviousTransactionContainsATokenToDelete",
                "has200StatusCodeOccurredPreviously", "has201StatusCodeOccurredPreviously", "has204StatusCodeOccurredPreviously",
                "has400StatusCodeOccurredPreviously", "has401StatusCodeOccurredPreviously", "has404StatusCodeOccurredPreviously",
                "has422StatusCodeOccurredPreviously", "has500StatusCodeOccurredPreviously", "has503StatusCodeOccurredPreviously",
                "hasAnyURLContainsATokenToCreate","hasAnyURLContainsATokenToRead", "hasAnyURLContainATokenToUpdate", "hasAnyURLContainATokenToDelete"
        });
        //responsebodykeylist.toString().replace("[","").replace("]","").replace(" ",""),

        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            List<String> method = new LinkedList<String>();
            List<String> code = new LinkedList<String>();
            List<String> action = new LinkedList<String>();

           for (List<HttpTransaction> mm : m.getValue().values()) {
                method.add(mm.get(0).getMethod());
                code.add(mm.get(0).getCode());
                action.add(mm.get(0).getURL());

                //Create record
                data.add(new String[]{
                       //method
                        String.valueOf(mm.get(0).getMethod()),
                        //request headers
                        String.valueOf(requestHeaders(mm,"Host")),
                        String.valueOf(requestHeaders(mm,"User-Agent")),
                        //response headers
                        String.valueOf(responseHeaders(mm,"cache-control")),
                        String.valueOf(responseHeaders(mm,"content-disposition")),
                        String.valueOf(responseHeaders(mm,"content-length")),
                        String.valueOf(responseHeaders(mm,"content-type")),
                        String.valueOf(responseHeaders(mm,"date")),
                        String.valueOf(responseHeaders(mm,"expires")),
                        String.valueOf(responseHeaders(mm,"last-modified")),
                        String.valueOf(responseHeaders(mm,"pragma")),
                        String.valueOf(responseHeaders(mm,"server")),
                        String.valueOf(responseHeaders(mm,"status")),
                        String.valueOf(responseHeaders(mm,"strict-transport-security")),
                        String.valueOf(responseHeaders(mm,"x-connection-hash")),
                        String.valueOf(responseHeaders(mm,"x-content-type-options")),
                        String.valueOf(responseHeaders(mm,"x-frame-options")),
                        String.valueOf(responseHeaders(mm,"x-rate-limit-limit")),
                        String.valueOf(responseHeaders(mm,"x-rate-limit-remaining")),
                        String.valueOf(responseHeaders(mm,"x-rate-limit-reset")),
                        String.valueOf(responseHeaders(mm,"x-response-time")),
                        String.valueOf(responseHeaders(mm,"x-transaction")),
                        String.valueOf(responseHeaders(mm,"x-twitter-response-tags")),
                        String.valueOf(responseHeaders(mm,"x-xss-protection")),
                        //statusCode
                        String.valueOf(mm.get(0).getCode()),
                        //responseBody
                        String.valueOf(body(mm,"contributors")),
                        String.valueOf(body(mm,"coordinates")),
                        String.valueOf(body(mm,"created_at")),
                        String.valueOf(bodyinside(mm,"entities","hashtags")),
                        String.valueOf(bodyinside(mm,"entities","symbols")),
                        String.valueOf(bodyinside(mm,"entities","urls")),
                        String.valueOf(bodyinside(mm,"entities","user_mentions")),
                        String.valueOf(bodyinsidearray(mm,"errors","code")),
                        String.valueOf(bodyinsidearray(mm,"errors","message")),
                        String.valueOf(body(mm,"favorite_count")),
                        String.valueOf(body(mm,"favorited")),
                        String.valueOf(body(mm,"geo")),
                        String.valueOf(body(mm,"id")),
                        String.valueOf(body(mm,"id_str")),
                        String.valueOf(body(mm,"in_reply_to_screen_name")),
                        String.valueOf(body(mm,"in_reply_to_status_id")),
                        String.valueOf(body(mm,"in_reply_to_status_id_str")),
                        String.valueOf(body(mm,"in_reply_to_user_id")),
                        String.valueOf(body(mm,"in_reply_to_user_id_str")),
                        String.valueOf(body(mm,"is_quote_status")),
                        String.valueOf(body(mm,"lang")),
                        String.valueOf(body(mm,"place")),
                        String.valueOf(body(mm,"retweet_count")),
                        String.valueOf(body(mm,"retweeted")),
                        //String.valueOf(body(mm,"source")),
                        String.valueOf(body(mm,"text")),
                        String.valueOf(body(mm,"truncated")),
                        String.valueOf(bodyinside(mm,"user","contributors_enabled")),
                        String.valueOf(bodyinside(mm,"user","created_at")),
                        String.valueOf(bodyinside(mm,"user","default_profile")),
                        String.valueOf(bodyinside(mm,"user","default_profile_image")),
                        String.valueOf(bodyinside(mm,"user","id")),
                        String.valueOf(bodyinside(mm,"user","id_str")),
                        String.valueOf(bodyinside(mm,"user","name")),
                        String.valueOf(bodyinside(mm,"user","screen_name")),
                        String.valueOf(bodyinside(mm,"user","location")),
                        String.valueOf(bodyinside(mm,"user","description")),
                        String.valueOf(bodyinside(mm,"user","url")),
                        String.valueOf(bodyinside(mm,"user","favourites_count")),
                        String.valueOf(bodyinside(mm,"user","follow_request_sent")),
                        String.valueOf(bodyinside(mm,"user","followers_count")),
                        String.valueOf(bodyinside(mm,"user","following")),
                        String.valueOf(bodyinside(mm,"user","friends_count")),
                        String.valueOf(bodyinside(mm,"user","geo_enabled")),
                        String.valueOf(bodyinside(mm,"user","has_extended_profile")),
                        String.valueOf(bodyinside(mm,"user","is_translation_enabled")),
                        String.valueOf(bodyinside(mm,"user","is_translator")),
                        String.valueOf(bodyinside(mm,"user","lang")),
                        String.valueOf(bodyinside(mm,"user","listed_count")),
                        String.valueOf(bodyinside(mm,"user","notifications")),
                        String.valueOf(bodyinside(mm,"user","profile_background_color")),
                        String.valueOf(bodyinside(mm,"user","profile_background_image_url")),
                        String.valueOf(bodyinside(mm,"user","profile_background_image_url_https")),
                        String.valueOf(bodyinside(mm,"user","profile_background_tile")),
                        String.valueOf(bodyinside(mm,"user","profile_banner_url")),
                        String.valueOf(bodyinside(mm,"user","profile_image_url")),
                        String.valueOf(bodyinside(mm,"user","profile_image_url_https")),
                        String.valueOf(bodyinside(mm,"user","profile_link_color")),
                        String.valueOf(bodyinside(mm,"user","profile_sidebar_border_color")),
                        String.valueOf(bodyinside(mm,"user","profile_sidebar_fill_color")),
                        String.valueOf(bodyinside(mm,"user","profile_text_color")),
                        String.valueOf(bodyinside(mm,"user","profile_use_background_image")),
                        String.valueOf(bodyinside(mm,"user","protected")),
                        String.valueOf(bodyinside(mm,"user","statuses_count")),
                        String.valueOf(bodyinside(mm,"user","time_zone")),
                        String.valueOf(bodyinside(mm,"user","translator_type")),
                        String.valueOf(bodyinside(mm,"user","utc_offset")),
                        String.valueOf(bodyinside(mm,"user","verified")),
                     //hasAuthorizationToken
                        String.valueOf(hasAuthorizationToken(mm)),
                        //URLScheme
                        String.valueOf(UrlTokenizer.getURLScheme(mm.get(0).getURL())),
                        //URLHost
                        String.valueOf(UrlTokenizer.getUriHost(mm.get(0).getURL())),
                        //URLpathtokens
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken1")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken2")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken3")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken4")),
                        //URLquerytokens
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken1")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken2")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken3")),
                        //URLfragmenttokens
                        String.valueOf(UrlTokenizer.getFragmentMap(mm.get(0).getURL()).get("fragmentToken1")),
                        // hasImmediatePreviousTransaction
                        String.valueOf(hasImmediatePreviousTransaction(code)),
                        //immediatelyPreviousStatusCode
                        String.valueOf(immediatelyPreviousStatusCode(code)),
                        // immediatelyPreviousMethod
                        String.valueOf(immediatelyPreviousMethod(method)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToCreate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToRead
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToRead(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToDelete
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(code,action)),
                        //has200StatusCodeOccurredPreviously
                        String.valueOf(has200StatusCodeOccurredPreviously(code)),
                        //has201OccurredPreviously
                        String.valueOf(has201StatusCodeOccurredPreviously(code)),
                        //has204OccurredPreviously
                        String.valueOf(has204StatusCodeOccurredPreviously(code)),
                        //has400OccurredPreviously
                        String.valueOf(has400StatusCodeOccurredPreviously(code)),
                        //has401OccurredPreviously
                        String.valueOf(has401StatusCodeOccurredPreviously(code)),
                        //has404OccurredPreviously
                        String.valueOf(has404StatusCodeOccurredPreviously(code)),
                        //has422OccurredPreviously
                        String.valueOf(has422StatusCodeOccurredPreviously(code)),
                        //has500OccurredPreviously
                        String.valueOf(has500StatusCodeOccurredPreviously(code)),
                        //has503StatusCodeOccurredPreviously
                        String.valueOf(has503StatusCodeOccurredPreviously(code)),
                        //hasAnyURLContainsATokenToCreate
                        String.valueOf(hasAnyURLContainsATokenToCreate(action)),
                        //hasAnyURLContainsATokenToRead
                        String.valueOf(hasAnyURLContainsATokenToRead(action)),
                        //hasAnyURLContainsATokenToUpdate
                        String.valueOf(hasAnyURLContainsATokenToUpdate(action)),
                        //hasAnyURLContainsATokenToDelete
                        String.valueOf(hasAnyURLContainsATokenToDelete(action))

                });
            }
        }

        //System.out.println(requestheaderskeylist);
        //System.out.println(responseheaderskeylist);
        //System.out.println(responsebodykeylist);


        // write out records
        writer.writeAll(data);
        //close the writer
        writer.close();
    }

    private static void extractKeySetForResponseHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                int index = mm.get(0).getResponseHeaders().indexOf("\t");
                String string_without_version = mm.get(0).getResponseHeaders().substring(index+1, mm.get(0).getResponseHeaders().length());
                String stuff_with_curlyB = "{"+string_without_version+"}";
                String reg = stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
                String value = reg.replace("\"[\"{", "[{").replace("~", "\":\"").replace("}\"]\"", "}]").replace("\"true\"", "true").replace("\"false\"", "false");
                System.out.println(value);
                //FeatureExtractor.extractKeys("responseheader",value.replaceAll("\t",","),responseheaderskeylist);
                 }
        }
    }

    private static void extractKeySetForRequestHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                String stuff_with_curlyB = "{"+mm.get(0).getRequestHeaders()+"}";
                String reg= stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
                String value=reg.replace("\"[\"{", "[{").replace("~","\":\"").replace("}\"]\"","}]").replace("\"true\"", "true").replace("\"false\"", "false");
                String header=value.replaceAll("\t",",");

                System.out.println(header);
                //FeatureExtractor.extractKeys("requestheader",value.replaceAll("\t",","),requestheaderskeylist);
                }
        }
    }

    private static void extractKeySetForResponseBody() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                FeatureExtractor.extractKeys("json",mm.get(0).getResponseBody(),responsebodykeylist);}
        }
    }

    public static boolean hasAuthorizationToken(List<HttpTransaction> mm) {
        boolean hasAuthorizationToken;
        if(String.valueOf(mm.get(0).getCode()).matches("401")){
            hasAuthorizationToken = false;
        }
        else{
            hasAuthorizationToken = true;
        }
        return hasAuthorizationToken;
    }

    public static String body(List<HttpTransaction> mm, String value) {
        String body = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
        //System.out.println(jsonObject);
        if(jsonObject.has(value)){
            body = jsonObject.get(value).toString();
        }

        return body;
    }

    public static String requestHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        //convert to json style
        String stuff_with_curlyB = "{"+mm.get(0).getRequestHeaders()+"}";
        //System.out.println(stuff_with_curlyB);
        //System.out.println("-------------------------------");
        String myVal = stuff_with_curlyB.replace("\"","\'");
        //System.out.println(myVal);

        String reg= myVal.replaceAll("[^\\{\\}\t]+", "\"$0\"");



        String vals=reg.replace("=\"","=").replace("\",",",");

        String val=vals.replace("\"[\"{", "[{").replace("~","\":\"").replace("}\"]\"","}]").replace("\"true\"", "true").replace("\"false\"", "false");

        String a=val.replaceAll("\t",",");

       JSONObject jsonObject = new JSONObject(a);
        if(jsonObject.has(value)){
            header = jsonObject.get(value).toString();
            //enclose entire field which contains commas to convert into .arff without any error
            if(header.indexOf(",") >= 0){
                header = "\'"+ header +"\'";
            }
        }

        return header;
    }

    public static String responseHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        //convert to json style
        int index = mm.get(0).getResponseHeaders().indexOf("\t");
        String string_without_version = mm.get(0).getResponseHeaders().substring(index+1, mm.get(0).getResponseHeaders().length());
        String stuff_with_curlyB = "{"+string_without_version+"}";
        String myVal = stuff_with_curlyB.replace("\"","\'");
        String reg = myVal.replaceAll("[^\\{\\}\t]+", "\"$0\"");
        String val = reg.replace("\"[\"{", "[{").replace("~", "\":\"").replace("}\"]\"", "}]").replace("\"true\"", "true").replace("\"false\"", "false");
        String a = val.replaceAll("\t",",");

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(a);
        JsonObject obj = element.getAsJsonObject();

        if(obj.has(value)){
            header = (String.valueOf(obj.get(value)).replaceAll("\"",""));

            if(header.indexOf(",") >= 0){
                header = "\'"+ header +"\'";
            }
        }

        return header;
    }

    public static String bodyinside(List<HttpTransaction> mm, String value, String subvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
        if(jsonObject.has(value)){

            JSONObject jsonObject2 = new JSONObject(jsonObject.get(value).toString());
            if(jsonObject2.has(subvalue)) {
                body = jsonObject2.get(subvalue).toString();
            }
        }

        return body;
    }

    public static String bodyinsideinside(List<HttpTransaction> mm, String value, String subvalue,String subsubvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
        if(jsonObject.has(value)){

            JSONObject jsonObject2 = new JSONObject(jsonObject.get(value).toString());
            if(jsonObject2.has(subvalue)) {

                JSONObject jsonObject3 = new JSONObject(jsonObject2.get(subvalue).toString());
                if(jsonObject3.has(subsubvalue)){
                    body = jsonObject3.get(subsubvalue).toString();
                }
            }
        }

        return body;
    }

    public static String bodyinsidearray(List<HttpTransaction> mm, String value, String subvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());

        if(jsonObject.has(value)) {
            JSONArray array = jsonObject.getJSONArray(value);


            JSONObject firstSport = array.getJSONObject(0);


            if (firstSport.has(subvalue)) {
                body = firstSport.get(subvalue).toString();
            }
        }


        return body;
    }

    public static boolean hasImmediatePreviousTransaction(List<String> n) {
        boolean hasImmediatePreviousTransaction = false;
        if (n.subList(0,n.size()-1).size() != 0) {
            hasImmediatePreviousTransaction = true;
        }
        else {
            hasImmediatePreviousTransaction = false;
        }
        return hasImmediatePreviousTransaction;
    }

    public static String immediatelyPreviousStatusCode(List<String> code) {
        String immediatelyPreviousStatusCode = "no-exist";

        if(code.subList(0,code.size()-1).size()!=0){

            immediatelyPreviousStatusCode = code.subList(0,code.size()-1).get(code.subList(0,code.size()-1).size()-1);
        }
        else {
            immediatelyPreviousStatusCode = "no-exist";
        }

        return immediatelyPreviousStatusCode;
    }

    public static String immediatelyPreviousMethod(List<String> n) {
        String method = "no-exist";

        if(n.subList(0,n.size()-1).size()!=0){
            method = n.subList(0,n.size()-1).get(n.subList(0,n.size()-1).size()-1);
        }
        else {
            method = "no-exist";
        }

        return method;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("update")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToRead(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("show")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("modify")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("destroy")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean has200StatusCodeOccurredPreviously(List<String> code) {
        boolean has200StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("200") )) {

            has200StatusCodeOccurredPreviously = true;
        }
        else {
            has200StatusCodeOccurredPreviously = false;
        }
        return has200StatusCodeOccurredPreviously;
    }

    public static boolean has201StatusCodeOccurredPreviously(List<String> code) {
        boolean has201StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("201") )) {

            has201StatusCodeOccurredPreviously = true;
        }
        else {
            has201StatusCodeOccurredPreviously = false;
        }
        return has201StatusCodeOccurredPreviously;
    }

    public static boolean has204StatusCodeOccurredPreviously(List<String> code) {
        boolean has204StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("204") )) {

            has204StatusCodeOccurredPreviously = true;
        }
        else {
            has204StatusCodeOccurredPreviously = false;
        }
        return has204StatusCodeOccurredPreviously;
    }

    public static boolean has400StatusCodeOccurredPreviously(List<String> code) {
        boolean has400StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("400") )) {

            has400StatusCodeOccurredPreviously = true;
        }
        else {
            has400StatusCodeOccurredPreviously = false;
        }
        return has400StatusCodeOccurredPreviously;
    }

    public static boolean has401StatusCodeOccurredPreviously(List<String> code) {
        boolean has401StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("401") )) {

            has401StatusCodeOccurredPreviously = true;
        }
        else {
            has401StatusCodeOccurredPreviously = false;
        }
        return has401StatusCodeOccurredPreviously;
    }

    public static boolean has404StatusCodeOccurredPreviously(List<String> code) {
        boolean has404StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("404") )) {

            has404StatusCodeOccurredPreviously = true;
        }
        else {
            has404StatusCodeOccurredPreviously = false;
        }
        return has404StatusCodeOccurredPreviously;
    }

    public static boolean has422StatusCodeOccurredPreviously(List<String> code) {
        boolean has422StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("422") )) {

            has422StatusCodeOccurredPreviously = true;
        }
        else {
            has422StatusCodeOccurredPreviously = false;
        }
        return has422StatusCodeOccurredPreviously;
    }

    public static boolean has500StatusCodeOccurredPreviously(List<String> code) {
        boolean has500StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("500") )) {

            has500StatusCodeOccurredPreviously = true;
        }
        else {
            has500StatusCodeOccurredPreviously = false;
        }
        return has500StatusCodeOccurredPreviously;
    }

    public static boolean has503StatusCodeOccurredPreviously(List<String> code) {
        boolean has503StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("503") )) {

            has503StatusCodeOccurredPreviously = true;
        }
        else {
            has503StatusCodeOccurredPreviously = false;
        }
        return has503StatusCodeOccurredPreviously;
    }

    public static boolean hasAnyURLContainsATokenToCreate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        //System.out.println(Urls);
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "update.json"));
        }
        //System.out.println(results.contains(true));
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToDelete(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "destroy"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToRead(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "show.json"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToUpdate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "modify"));
        }
        return results.contains(true);
    }



}
