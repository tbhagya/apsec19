package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import com.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.*;


/**
 * This class generates csv file for GHTraffic dataset
 * related with attributes
 * @author thilini bhagya
 */
public class CsvFileGeneratorForGHTraffic {
    static Set<String> responsebodykeylist = new TreeSet<String>();
    static Set<String> requestheaderskeylist = new TreeSet<String>();
    static Set<String> responseheaderskeylist = new TreeSet<String>();

    public static void csvFileGeneratorWithAttributes(String writeFileName, String readFileName) throws Exception {
        String csv = writeFileName + ".csv";
        CSVWriter writer = new CSVWriter(new FileWriter(csv), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        List<String[]> data = new ArrayList<String[]>();

        HttpTransaction.read(readFileName + ".csv");

        //extract key set for responseBody
        //extractKeySetForResponseBody();
        //extract key set for headers
        //extractKeySetForRequestHeaders();
        //extractKeySetForResponseHeaders();

        //add the header line
        data.add(new String[]{
                "method","requestHeader:Accept","requestHeader:Content-Length",
                "requestHeader:Content-Type","requestHeader:Host",
                "requestHeader:User-Agent",
                "hasPayload", "hasValidPayload",
                "requestBody:state",
                //"requestBody:title",
                "responseheader:Access-Control-Allow-Origin","responseheader:Access-Control-Expose-Headers",
                "responseheader:Cache-Control","responseheader:Content-Length","responseheader:Content-Type",
                "responseheader:Date","responseheader:ETag","responseheader:Last-Modified","responseheader:Location",
                "responseheader:Server","responseheader:Vary","responseheader:X-Accepted-OAuth-Scopes",
                "responseheader:X-GitHub-Media-Type","responseheader:X-GitHub-Request-Id","responseheader:X-OAuth-Scopes",
                //"json:assignee",
                "json:assignee.avatar_url",
                "json:assignee.events_url","json:assignee.followers_url",
                "json:assignee.following_url","json:assignee.gists_url","json:assignee.gravatar_id","json:assignee.html_url",
                "json:assignee.id","json:assignee.login","json:assignee.organizations_url","json:assignee.received_events_url",
                "json:assignee.repos_url","json:assignee.site_admin", "json:assignee.starred_url", "json:assignee.subscriptions_url",
                "json:assignee.type", "json:assignee.url",
                "json:assignees.avatar_url", "json:assignees.events_url", "json:assignees.followers_url",
                "json:assignees.following_url", "json:assignees.gists_url", "json:assignees.gravatar_id", "json:assignees.html_url",
                "json:assignees.id", "json:assignees.login", "json:assignees.organizations_url", "json:assignees.received_events_url",
                "json:assignees.repos_url", "json:assignees.site_admin", "json:assignees.starred_url", "json:assignees.subscriptions_url",
                "json:assignees.type", "json:assignees.url",
                "json:closed_by.avatar_url", "json:closed_by.events_url", "json:closed_by.followers_url",
                "json:closed_by.following_url", "json:closed_by.gists_url", "json:closed_by.gravatar_id", "json:closed_by.html_url",
                "json:closed_by.id", "json:closed_by.login", "json:closed_by.organizations_url", "json:closed_by.received_events_url",
                "json:closed_by.repos_url", "json:closed_by.site_admin", "json:closed_by.starred_url", "json:closed_by.subscriptions_url",
                "json:closed_by.type", "json:closed_by.url",
                "json:user.avatar_url", "json:user.events_url", "json:user.followers_url",
                "json:user.following_url", "json:user.gists_url", "json:user.gravatar_id", "json:user.html_url",
                "json:user.id", "json:user.login", "json:user.organizations_url", "json:user.received_events_url",
                "json:user.repos_url", "json:user.site_admin", "json:user.starred_url", "json:user.subscriptions_url",
                "json:user.type", "json:user.url",
                //"json:body",
                "json:closed_at",
                "json:comments", "json:created_at",
                "json:documentation_url", "json:html_url", "json:id",
                "json:number", "json:state",
                //"json:title",
                "json:updated_at", "json:url",
                //"json:labels.url","json:labels.name","json:labels.color",
                "json:milestone.closed_at", "json:milestone.closed_issues", "json:milestone.created_at",
                "json:milestone.description", "json:milestone.due_on", "json:milestone.html_url", "json:milestone.id",
                "json:milestone.labels_url",
                "json:milestone.number", "json:milestone.open_issues", "json:milestone.state",
                "json:milestone.title", "json:milestone.updated_at",
                "json:milestone.url",
                "json:milestone.creator.avatar_url", "json:milestone.creator.events_url", "json:milestone.creator.followers_url",
                "json:milestone.creator.following_url", "json:milestone.creator.gists_url", "json:milestone.creator.gravatar_id", "json:milestone.creator.html_url",
                "json:milestone.creator.id", "json:milestone.creator.login", "json:milestone.creator.organizations_url", "json:milestone.creator.received_events_url",
                "json:milestone.creator.repos_url", "json:milestone.creator.site_admin", "json:milestone.creator.starred_url", "json:milestone.creator.subscriptions_url",
                "json:milestone.creator.type", "json:milestone.creator.url",
                "json:locked",
                //"json:message",
                "statusCode",
                "hasAuthorizationToken",
                "uriSchema", "uriHost",
                "uriPathToken1", "uriPathToken2", "uriPathToken3", "uriPathToken4",
                "uriQueryToken1", "uriQueryToken2", "uriQueryToken3",
                "uriFragmentToken1",
                "hasImmediatePreviousTransaction", "immediatelyPreviousStatusCode", "immediatelyPreviousMethod",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToCreate", "hasURLInImmediatelyPreviousTransactionContainsATokenToRead",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate", "hasURLInImmediatelyPreviousTransactionContainsATokenToDelete",
                "has200StatusCodeOccurredPreviously", "has201StatusCodeOccurredPreviously", "has204StatusCodeOccurredPreviously",
                "has400StatusCodeOccurredPreviously", "has401StatusCodeOccurredPreviously", "has404StatusCodeOccurredPreviously",
                "has422StatusCodeOccurredPreviously", "has500StatusCodeOccurredPreviously", "has503StatusCodeOccurredPreviously",
                "hasAnyURLContainsATokenToCreate", "hasAnyURLContainsATokenToRead", "hasAnyURLContainATokenToUpdate", "hasAnyURLContainATokenToDelete"


        });


        //responsebodykeylist.toString().replace("[","").replace("]","").replace(" ",""),

        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()) {
            List<String> method = new LinkedList<String>();
            List<String> code = new LinkedList<String>();
            List<String> action = new LinkedList<String>();

            for (List<HttpTransaction> mm : m.getValue().values()) {
                method.add(mm.get(0).getMethod());
                code.add(mm.get(0).getCode());
                action.add(mm.get(0).getURL());

                //Create record
                data.add(new String[]{
                        //method
                        String.valueOf(mm.get(0).getMethod()),
                        //request headers
                        String.valueOf(requestHeaders(mm, "Accept")),
                        String.valueOf(requestHeaders(mm, "Content-Length")),
                        String.valueOf(requestHeaders(mm, "Content-Type")),
                        String.valueOf(requestHeaders(mm, "Host")),
                        String.valueOf(requestHeaders(mm, "User-Agent")),
                        //hasPayload
                        String.valueOf(hasPayload(mm)),
                        //hasValidPayload
                        String.valueOf(hasValidJson(mm)),
                        //request body
                        String.valueOf(requestbody(mm, "state")),
                        //String.valueOf(requestbody(mm,"title")),
                        //response headers
                        String.valueOf(responseHeaders(mm, "Access-Control-Allow-Origin")),
                        String.valueOf(responseHeaders(mm, "Access-Control-Expose-Headers")),
                        String.valueOf(responseHeaders(mm, "Cache-Control")),
                        String.valueOf(responseHeaders(mm, "Content-Length")),
                        String.valueOf(responseHeaders(mm, "Content-Type")),
                        String.valueOf(responseHeaders(mm, "Date")),
                        String.valueOf(responseHeaders(mm, "ETag")),
                        String.valueOf(responseHeaders(mm, "Last-Modified")),
                        String.valueOf(responseHeaders(mm, "Location")),
                        String.valueOf(responseHeaders(mm, "Server")),
                        String.valueOf(responseHeaders(mm, "Vary")),
                        String.valueOf(responseHeaders(mm, "X-Accepted-OAuth-Scopes")),
                        String.valueOf(responseHeaders(mm, "X-GitHub-Media-Type")),
                        String.valueOf(responseHeaders(mm, "X-GitHub-Request-Id")),
                        String.valueOf(responseHeaders(mm, "X-OAuth-Scopes")),
                        //String.valueOf(body(mm,"assignee")),
                        String.valueOf(bodyinside(mm, "assignee", "avatar_url")),
                        String.valueOf(bodyinside(mm, "assignee", "events_url")),
                        String.valueOf(bodyinside(mm, "assignee", "followers_url")),
                        String.valueOf(bodyinside(mm, "assignee", "following_url")),
                        String.valueOf(bodyinside(mm, "assignee", "gists_url")),
                        String.valueOf(bodyinside(mm, "assignee", "gravatar_id")),
                        String.valueOf(bodyinside(mm, "assignee", "html_url")),
                        String.valueOf(bodyinside(mm, "assignee", "id")),
                        String.valueOf(bodyinside(mm, "assignee", "login")),
                        String.valueOf(bodyinside(mm, "assignee", "organizations_url")),
                        String.valueOf(bodyinside(mm, "assignee", "received_events_url")),
                        String.valueOf(bodyinside(mm, "assignee", "repos_url")),
                        String.valueOf(bodyinside(mm, "assignee", "site_admin")),
                        String.valueOf(bodyinside(mm, "assignee", "starred_url")),
                        String.valueOf(bodyinside(mm, "assignee", "subscriptions_url")),
                        String.valueOf(bodyinside(mm, "assignee", "type")),
                        String.valueOf(bodyinside(mm, "assignee", "url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "avatar_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "events_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "followers_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "following_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "gists_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "gravatar_id")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "html_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "id")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "login")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "organizations_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "received_events_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "repos_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "site_admin")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "starred_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "subscriptions_url")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "type")),
                        String.valueOf(bodyinsidearray(mm, "assignees", "url")),
                        String.valueOf(bodyinside(mm, "closed_by", "avatar_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "events_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "followers_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "following_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "gists_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "gravatar_id")),
                        String.valueOf(bodyinside(mm, "closed_by", "html_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "id")),
                        String.valueOf(bodyinside(mm, "closed_by", "login")),
                        String.valueOf(bodyinside(mm, "closed_by", "organizations_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "received_events_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "repos_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "site_admin")),
                        String.valueOf(bodyinside(mm, "closed_by", "starred_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "subscriptions_url")),
                        String.valueOf(bodyinside(mm, "closed_by", "type")),
                        String.valueOf(bodyinside(mm, "closed_by", "url")),
                        String.valueOf(bodyinside(mm, "user", "avatar_url")),
                        String.valueOf(bodyinside(mm, "user", "events_url")),
                        String.valueOf(bodyinside(mm, "user", "followers_url")),
                        String.valueOf(bodyinside(mm, "user", "following_url")),
                        String.valueOf(bodyinside(mm, "user", "gists_url")),
                        String.valueOf(bodyinside(mm, "user", "gravatar_id")),
                        String.valueOf(bodyinside(mm, "user", "html_url")),
                        String.valueOf(bodyinside(mm, "user", "id")),
                        String.valueOf(bodyinside(mm, "user", "login")),
                        String.valueOf(bodyinside(mm, "user", "organizations_url")),
                        String.valueOf(bodyinside(mm, "user", "received_events_url")),
                        String.valueOf(bodyinside(mm, "user", "repos_url")),
                        String.valueOf(bodyinside(mm, "user", "site_admin")),
                        String.valueOf(bodyinside(mm, "user", "starred_url")),
                        String.valueOf(bodyinside(mm, "user", "subscriptions_url")),
                        String.valueOf(bodyinside(mm, "user", "type")),
                        String.valueOf(bodyinside(mm, "user", "url")),
                        //String.valueOf(body(mm,"body")),
                        String.valueOf(body(mm, "closed_at")),
                        String.valueOf(body(mm, "comments")),
                        String.valueOf(body(mm, "created_at")),
                        String.valueOf(body(mm, "documentation_url")),
                        String.valueOf(body(mm, "html_url")),
                        String.valueOf(body(mm, "id")),
                        String.valueOf(body(mm, "number")),
                        String.valueOf(body(mm, "state")),
                        //String.valueOf(body(mm,"title")),
                        String.valueOf(body(mm, "updated_at")),
                        String.valueOf(body(mm, "url")),
                        //String.valueOf(bodyinsidearray(mm,"labels","url")),
                        //String.valueOf(bodyinsidearray(mm,"labels","name")),
                        //String.valueOf(bodyinsidearray(mm,"labels","color")),
                        String.valueOf(bodyinside(mm, "milestone", "closed_at")),
                        String.valueOf(bodyinside(mm, "milestone", "closed_issues")),
                        String.valueOf(bodyinside(mm, "milestone", "created_at")),
                        //String.valueOf(bodyinside(mm,"milestone","creator")),
                        String.valueOf(bodyinside(mm, "milestone", "description")),
                        String.valueOf(bodyinside(mm, "milestone", "due_on")),
                        String.valueOf(bodyinside(mm, "milestone", "html_url")),
                        String.valueOf(bodyinside(mm, "milestone", "id")),
                        String.valueOf(bodyinside(mm, "milestone", "labels_url")),
                        String.valueOf(bodyinside(mm, "milestone", "number")),
                        String.valueOf(bodyinside(mm, "milestone", "open_issues")),
                        String.valueOf(bodyinside(mm, "milestone", "state")),
                        String.valueOf(bodyinside(mm, "milestone", "title")),
                        String.valueOf(bodyinside(mm, "milestone", "updated_at")),
                        String.valueOf(bodyinside(mm, "milestone", "url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "avatar_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "events_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "followers_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "following_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "gists_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "gravatar_id")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "html_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "id")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "login")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "organizations_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "received_events_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "repos_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "site_admin")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "starred_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "subscriptions_url")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "type")),
                        String.valueOf(bodyinsideinside(mm, "milestone", "creator", "url")),
                        String.valueOf(body(mm, "locked")),
                        //String.valueOf(body(mm,"message")),
                        //statusCode
                        String.valueOf(mm.get(0).getCode()),
                        String.valueOf(hasAuthorizationToken(mm)),
                        //URLScheme
                        String.valueOf(UrlTokenizer.getURLScheme(mm.get(0).getURL())),
                        //URLHost
                        String.valueOf(UrlTokenizer.getUriHost(mm.get(0).getURL())),
                        //URLpathtokens
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken1")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken2")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken3")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken4")),
                        //URLquerytokens
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken1")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken2")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken3")),
                        //URLfragmenttokens
                        String.valueOf(UrlTokenizer.getFragmentMap(mm.get(0).getURL()).get("fragmentToken1")),
                        // hasImmediatePreviousTransaction
                        String.valueOf(hasImmediatePreviousTransaction(code)),
                        //immediatelyPreviousStatusCode
                        String.valueOf(immediatelyPreviousStatusCode(code)),
                        // immediatelyPreviousMethod
                        String.valueOf(immediatelyPreviousMethod(method)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToCreate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(code, action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToRead
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToRead(code, action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(code, action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToDelete
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(code, action)),
                        //has200StatusCodeOccurredPreviously
                        String.valueOf(has200StatusCodeOccurredPreviously(code)),
                        //has201OccurredPreviously
                        String.valueOf(has201StatusCodeOccurredPreviously(code)),
                        //has204OccurredPreviously
                        String.valueOf(has204StatusCodeOccurredPreviously(code)),
                        //has400OccurredPreviously
                        String.valueOf(has400StatusCodeOccurredPreviously(code)),
                        //has401OccurredPreviously
                        String.valueOf(has401StatusCodeOccurredPreviously(code)),
                        //has404OccurredPreviously
                        String.valueOf(has404StatusCodeOccurredPreviously(code)),
                        //has422OccurredPreviously
                        String.valueOf(has422StatusCodeOccurredPreviously(code)),
                        //has500OccurredPreviously
                        String.valueOf(has500StatusCodeOccurredPreviously(code)),
                        //has503StatusCodeOccurredPreviously
                        String.valueOf(has503StatusCodeOccurredPreviously(code)),
                        //hasAnyURLContainsATokenToCreate
                        String.valueOf(hasAnyURLContainsATokenToCreate(action)),
                        //hasAnyURLContainsATokenToRead
                        String.valueOf(hasAnyURLContainsATokenToRead(action)),
                        //hasAnyURLContainsATokenToUpdate
                        String.valueOf(hasAnyURLContainsATokenToUpdate(action)),
                        //hasAnyURLContainsATokenToDelete
                        String.valueOf(hasAnyURLContainsATokenToDelete(action))

                });
            }
        }

        //System.out.println(requestheaderskeylist);
        //System.out.println(responseheaderskeylist);
        //System.out.println(responsebodykeylist);


        // write out records
        writer.writeAll(data);
        //close the writer
        writer.close();
    }

    private static void extractKeySetForResponseHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()) {
            for (List<HttpTransaction> mm : m.getValue().values()) {
                FeatureExtractor.extractKeys("responseheader", mm.get(0).getResponseHeaders(), responseheaderskeylist);
            }
        }
    }

    private static void extractKeySetForRequestHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()) {
            for (List<HttpTransaction> mm : m.getValue().values()) {
                FeatureExtractor.extractKeys("json", mm.get(0).getRequestHeaders(), requestheaderskeylist);
            }
        }


    }

    private static void extractKeySetForResponseBody() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()) {
            for (List<HttpTransaction> mm : m.getValue().values()) {
                String string_without_version = mm.get(0).getResponseBody();
                if (string_without_version.matches("no-exist")) {
                } else {
                    FeatureExtractor.extractKeys("json", string_without_version, responsebodykeylist);
                    //System.out.println(string_without_version);
                }
            }
        }
    }

    public static boolean hasAuthorizationToken(List<HttpTransaction> mm) {
        boolean hasAuthorizationToken;
        if (String.valueOf(mm.get(0).getCode()).matches("401")) {
            hasAuthorizationToken = false;
        } else {
            hasAuthorizationToken = true;
        }
        return hasAuthorizationToken;
    }

    public static String body(List<HttpTransaction> mm, String value) {
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody();
        if (string_without_version.matches("null")) {
        } else {
            JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
            //System.out.println(jsonObject);
            if (jsonObject.has(value)) {

                body = jsonObject.get(value).toString();
            }
        }
        return body;
    }

    public static String requestbody(List<HttpTransaction> mm, String value) {
        String body = "no-exist";
        String string_without_version = mm.get(0).getRequestBody();
        if (string_without_version.matches("null")) {
        } else {
            JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());

            if (jsonObject.has(value)) {
/*
                body = (String.valueOf(jsonObject.get(value)).replaceAll("\"",""));
                System.out.println(body);
                if(body.indexOf(",") >= 0){
                    body = "\'"+ body +"\'";*/


                body = (String) jsonObject.get(value);
            }
            //}
        }
        return body;
    }

    public static String requestHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getRequestHeaders());
        if (jsonObject.has(value)) {
            header = (String.valueOf(jsonObject.get(value)).replaceAll("\"", ""));

            if(header.indexOf(",") >= 0){
                header = "\'"+ header +"\'";
            }

        }
        return header;
    }

    public static String responseHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        JSONObject jsonObject = new JSONObject(mm.get(0).getResponseHeaders());
        if (jsonObject.has(value)) {
            header = (String.valueOf(jsonObject.get(value)).replaceAll("\"", ""));

            if(header.indexOf(",") >= 0){
                header = "\'"+ header +"\'";
            }
        }
        return header;
    }

    public static String bodyinside(List<HttpTransaction> mm, String value, String subvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody();
        if (string_without_version.matches("null")) {

        } else {
            //System.out.println(mm.get(0).getResponseBody());
            JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
            if (jsonObject.has(value)) {
                String a = jsonObject.get(value).toString();
                if (a.matches("null")) {

                } else if (jsonObject.has(value)) {
                    JSONObject jsonObject2 = new JSONObject(jsonObject.get(value).toString());
                    if (jsonObject2.has(subvalue)) {
                        body = jsonObject2.get(subvalue).toString();
                    }
                }
                //System.out.println(jsonObject.get(value).toString());

            }
        }

        return body;
    }

    public static String bodyinsideinside(List<HttpTransaction> mm, String value, String subvalue, String subsubvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody();
        if (string_without_version.matches("null")) {
        } else {
            JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());
            if (jsonObject.has(value)) {
                String a = jsonObject.get(value).toString();
                if (a.matches("null")) {
                } else {
                    JSONObject jsonObject2 = new JSONObject(jsonObject.get(value).toString());
                    if (jsonObject2.has(subvalue)) {

                        String ab = jsonObject.get(value).toString();
                        if (ab.matches("null")) {

                        } else {
                            JSONObject jsonObject3 = new JSONObject(jsonObject2.get(subvalue).toString());
                            if (jsonObject3.has(subsubvalue)) {
                                body = jsonObject3.get(subsubvalue).toString();
                            }
                        }
                    }
                }
            }
        }

        return body;
    }

    public static String bodyinsidearray(List<HttpTransaction> mm, String value, String subvalue) {
        //System.out.println(mm.get(0).getResponseBody());
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody();
        if (string_without_version.matches("null")) {
        } else {
            JSONObject jsonObject = new JSONObject(mm.get(0).getResponseBody());

            if (jsonObject.has(value)) {

                if (!jsonObject.get(value).toString().contains("[]")) {
                    JSONArray array = jsonObject.getJSONArray(value);


                    JSONObject firstSport = array.getJSONObject(0);


                    if (firstSport.has(subvalue)) {
                        body = firstSport.get(subvalue).toString();
                    }

                }
            }
        }


        return body;
    }

    public static boolean hasImmediatePreviousTransaction(List<String> n) {
        boolean hasImmediatePreviousTransaction = false;
        if (n.subList(0, n.size() - 1).size() != 0) {
            hasImmediatePreviousTransaction = true;
        } else {
            hasImmediatePreviousTransaction = false;
        }
        return hasImmediatePreviousTransaction;
    }

    public static String immediatelyPreviousStatusCode(List<String> code) {
        String immediatelyPreviousStatusCode = "no-exist";

        if (code.subList(0, code.size() - 1).size() != 0) {

            immediatelyPreviousStatusCode = code.subList(0, code.size() - 1).get(code.subList(0, code.size() - 1).size() - 1);
        } else {
            immediatelyPreviousStatusCode = "no-exist";
        }

        return immediatelyPreviousStatusCode;
    }

    public static String immediatelyPreviousMethod(List<String> n) {
        String method = "no-exist";

        if (n.subList(0, n.size() - 1).size() != 0) {
            method = n.subList(0, n.size() - 1).get(n.subList(0, n.size() - 1).size() - 1);
        } else {
            method = "no-exist";
        }

        return method;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(List<String> code, List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if (code.subList(0, code.size() - 1).size() != 0) {


            if (((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("update")))) {
                hasImmediatePreviousTransaction = true;
            }
        } else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToRead(List<String> code, List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if (code.subList(0, code.size() - 1).size() != 0) {


            if (((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("show")))) {
                hasImmediatePreviousTransaction = true;
            }
        } else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(List<String> code, List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if (code.subList(0, code.size() - 1).size() != 0) {


            if (((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("modify")))) {
                hasImmediatePreviousTransaction = true;
            }
        } else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(List<String> code, List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if (code.subList(0, code.size() - 1).size() != 0) {


            if (((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("destroy")))) {
                hasImmediatePreviousTransaction = true;
            }
        } else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean has200StatusCodeOccurredPreviously(List<String> code) {
        boolean has200StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("200"))) {

            has200StatusCodeOccurredPreviously = true;
        } else {
            has200StatusCodeOccurredPreviously = false;
        }
        return has200StatusCodeOccurredPreviously;
    }

    public static boolean has201StatusCodeOccurredPreviously(List<String> code) {
        boolean has201StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("201"))) {

            has201StatusCodeOccurredPreviously = true;
        } else {
            has201StatusCodeOccurredPreviously = false;
        }
        return has201StatusCodeOccurredPreviously;
    }

    public static boolean has204StatusCodeOccurredPreviously(List<String> code) {
        boolean has204StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("204"))) {

            has204StatusCodeOccurredPreviously = true;
        } else {
            has204StatusCodeOccurredPreviously = false;
        }
        return has204StatusCodeOccurredPreviously;
    }

    public static boolean has400StatusCodeOccurredPreviously(List<String> code) {
        boolean has400StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("400"))) {

            has400StatusCodeOccurredPreviously = true;
        } else {
            has400StatusCodeOccurredPreviously = false;
        }
        return has400StatusCodeOccurredPreviously;
    }

    public static boolean has401StatusCodeOccurredPreviously(List<String> code) {
        boolean has401StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("401"))) {

            has401StatusCodeOccurredPreviously = true;
        } else {
            has401StatusCodeOccurredPreviously = false;
        }
        return has401StatusCodeOccurredPreviously;
    }

    public static boolean has404StatusCodeOccurredPreviously(List<String> code) {
        boolean has404StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("404"))) {

            has404StatusCodeOccurredPreviously = true;
        } else {
            has404StatusCodeOccurredPreviously = false;
        }
        return has404StatusCodeOccurredPreviously;
    }

    public static boolean has422StatusCodeOccurredPreviously(List<String> code) {
        boolean has422StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("422"))) {

            has422StatusCodeOccurredPreviously = true;
        } else {
            has422StatusCodeOccurredPreviously = false;
        }
        return has422StatusCodeOccurredPreviously;
    }

    public static boolean has500StatusCodeOccurredPreviously(List<String> code) {
        boolean has500StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("500"))) {

            has500StatusCodeOccurredPreviously = true;
        } else {
            has500StatusCodeOccurredPreviously = false;
        }
        return has500StatusCodeOccurredPreviously;
    }

    public static boolean has503StatusCodeOccurredPreviously(List<String> code) {
        boolean has503StatusCodeOccurredPreviously = false;
        if (code.subList(0, code.size() - 1).contains(("503"))) {

            has503StatusCodeOccurredPreviously = true;
        } else {
            has503StatusCodeOccurredPreviously = false;
        }
        return has503StatusCodeOccurredPreviously;
    }

    public static boolean hasAnyURLContainsATokenToCreate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0, action.size() - 1);
        List results = new LinkedList<String>();
        //System.out.println(Urls);
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "update.json"));
        }
        //System.out.println(results.contains(true));
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToDelete(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0, action.size() - 1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "destroy"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToRead(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0, action.size() - 1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "show.json"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToUpdate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0, action.size() - 1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "modify"));
        }
        return results.contains(true);
    }


    //need to write a method to check json responseBody
    public static String hasPayload(List<HttpTransaction> mm) {
        String hasPayload;
        if (((String.valueOf(mm.get(0).getMethod()).matches("POST")) || (String.valueOf(mm.get(0).getMethod()).matches("PATCH"))) && ((String.valueOf(mm.get(0).getCode()).matches("201")) || ((String.valueOf(mm.get(0).getCode()).matches("404"))) || ((String.valueOf(mm.get(0).getCode()).matches("200"))) || ((String.valueOf(mm.get(0).getCode()).matches("400"))) || ((String.valueOf(mm.get(0).getCode()).matches("401"))))) {
            hasPayload = "true";

        } else if (((String.valueOf(mm.get(0).getMethod()).matches("POST")) || (String.valueOf(mm.get(0).getMethod()).matches("PATCH"))) && ((String.valueOf(mm.get(0).getCode()).matches("422")))) {
            hasPayload = "false";

        } else {
            hasPayload = "false";
        }
        return hasPayload;
    }

    //need to write a method to check json responseBody
    public static String hasValidJson(List<HttpTransaction> mm) {
        String hasValidJson = "false";
        if (hasPayload(mm) == "true") {
            if (((String.valueOf(mm.get(0).getMethod()).matches("POST")) || (String.valueOf(mm.get(0).getMethod()).matches("PATCH"))) && String.valueOf(mm.get(0).getCode()).matches("400")) {
                hasValidJson = "false";
            } else if (((String.valueOf(mm.get(0).getMethod()).matches("POST")) || (String.valueOf(mm.get(0).getMethod()).matches("PATCH"))) && ((String.valueOf(mm.get(0).getCode()).matches("201")) || (String.valueOf(mm.get(0).getCode()).matches("404")) || (String.valueOf(mm.get(0).getCode()).matches("200")))) {
                hasValidJson = "true";
            }

        } else if (hasPayload(mm) == "false") {
            hasValidJson = "false";
        } else {
            hasValidJson = "false";
        }
        return hasValidJson;
    }


}

