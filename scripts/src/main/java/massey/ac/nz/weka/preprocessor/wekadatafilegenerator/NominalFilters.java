package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.StringToNominal;

/**
 * This class converts int and string attributes to nominal
 * @author thilini bhagya
 */
public class NominalFilters {

    public static Instances getDataInstance(Instances fileName, int index) throws Exception {

        //convert numeric values in column 3 (status code) into nominal
        NumericToNominal convert= new NumericToNominal();
        String[] options= new String[2];
        options[0]="-R";
        options[1]= String.valueOf(index + 1);  //variable in column 3 to make numeric

        convert.setOptions(options);
        convert.setInputFormat(fileName);

        Instances newData= Filter.useFilter(fileName, convert);
        newData.setClassIndex(index);

        return newData;

    }

    public static Instances getDataStringInstance(Instances fileName, int index) throws Exception {

        //convert numeric values in column 3 (status code) into nominal
        StringToNominal convert = new StringToNominal();
        String[] options= new String[2];
        options[0]="-R";
        options[1]= String.valueOf(index + 1);  //variable in column 3 to make numeric

        convert.setOptions(options);
        convert.setInputFormat(fileName);

        Instances newData= Filter.useFilter(fileName, convert);
        newData.setClassIndex(index);

        return newData;

    }

}
