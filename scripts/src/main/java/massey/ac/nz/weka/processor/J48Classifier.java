package massey.ac.nz.weka.processor;

import massey.ac.nz.weka.preprocessor.wekadatafilegenerator.NominalFilters;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import java.util.*;
import java.awt.*;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

/**
 * This class trains models with J48 classification algorithm
 * @author thilini bhagya
 */

public class J48Classifier {


    public static void processWithJ48(String dataFileName, int indexToLearn, int[] indicesToRemove) throws Exception {
        try{
        System.out.println("J48 Classifier for "+ dataFileName.substring(14));
        Instances trainingDataSet = new ConverterUtils.DataSource(dataFileName + ".arff").getDataSet();
        System.out.println(trainingDataSet.attribute(indexToLearn));

        //check attribute type of the target
        if (trainingDataSet.attribute(indexToLearn).isNumeric()) {
            trainingDataSet = NominalFilters.getDataInstance(trainingDataSet, indexToLearn);
        } else if (trainingDataSet.attribute(indexToLearn).isString()) {
            trainingDataSet = NominalFilters.getDataStringInstance(trainingDataSet, indexToLearn);
        } else {
            trainingDataSet.setClassIndex(indexToLearn);
        }

        //remove attributes related to response properties
        Remove removeFilter = new Remove();
        removeFilter.setAttributeIndicesArray(indicesToRemove);
        removeFilter.setInputFormat(trainingDataSet);
        Instances newTrainingDataSet = Filter.useFilter(trainingDataSet, removeFilter);

        //build classifier
        J48 classifier = new J48();


        classifier.buildClassifier(newTrainingDataSet);
        System.out.println(classifier);

        //evaluate using cross validation
        Evaluation eval = new Evaluation(newTrainingDataSet);
        eval.crossValidateModel(classifier, newTrainingDataSet, 10, new Random(1));
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toClassDetailsString());

        //print model
        final javax.swing.JFrame jf = new javax.swing.JFrame("Weka Classifier Tree Visualizer: J48");
        jf.setSize(2000, 1000);
        jf.getContentPane().setLayout(new BorderLayout());
        TreeVisualizer tv = new TreeVisualizer(null, classifier.graph(), new PlaceNode2());
        jf.getContentPane().add(tv, BorderLayout.CENTER);
        jf.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                jf.dispose();
            }
        });
        jf.setVisible(true);
        tv.fitToScreen();}

        //handle if selected target is unary
        catch (Exception e) {
            if(e.getMessage().matches("weka.classifiers.trees.j48.C45PruneableClassifierTree: Cannot handle unary class!"));
            System.out.println("target is a unary class, cannot handle using Weka");
        }

    }
}
