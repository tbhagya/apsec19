package massey.ac.nz.weka.processor;

import massey.ac.nz.weka.preprocessor.wekadatafilegenerator.NominalFilters;
import weka.classifiers.Evaluation;
import weka.classifiers.rules.PART;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import java.util.*;

/**
 * This class trains models with PART classification algorithm
 * @author thilini bhagya
 */
public class PARTClassifier {
    public static void processWithPART(String dataFileName, int indexToLearn, int[] indicesToRemove) throws Exception {
        try {
            System.out.println("PART Classifier for " + dataFileName.substring(14));
            Instances trainingDataSet = new ConverterUtils.DataSource(dataFileName + ".arff").getDataSet();
            System.out.println(trainingDataSet.attribute(indexToLearn));

            //check attribute type of the target
            if (trainingDataSet.attribute(indexToLearn).isNumeric()) {
                trainingDataSet = NominalFilters.getDataInstance(trainingDataSet, indexToLearn);
            } else if (trainingDataSet.attribute(indexToLearn).isString()) {
                trainingDataSet = NominalFilters.getDataStringInstance(trainingDataSet, indexToLearn);
            } else {
                trainingDataSet.setClassIndex(indexToLearn);
            }

            //remove attributes related to response properties
            Remove removeFilter = new Remove();
            removeFilter.setAttributeIndicesArray(indicesToRemove);
            removeFilter.setInputFormat(trainingDataSet);
            Instances newTrainingDataSet = Filter.useFilter(trainingDataSet, removeFilter);

            //build classifier
            PART classifier = new PART();


            classifier.buildClassifier(newTrainingDataSet);
            System.out.println(classifier);

            //evaluate using cross validation
            Evaluation eval = new Evaluation(newTrainingDataSet);
            eval.crossValidateModel(classifier, newTrainingDataSet, 10, new Random(1));
            System.out.println(eval.toSummaryString());
            System.out.println(eval.toClassDetailsString());
        }

        //handle if selected target is unary
        catch(Exception e){
            if (e.getMessage().matches("weka.classifiers.trees.part.C45PruneableClassifierTree: Cannot handle unary class!"))
                System.out.println("target is a unary class, cannot handle using Weka");
        }
    }
}
