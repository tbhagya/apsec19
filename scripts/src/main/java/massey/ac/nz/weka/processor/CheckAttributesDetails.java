package massey.ac.nz.weka.processor;

import massey.ac.nz.weka.Logging;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * this class allows to check what are the attributes and their indexes
 * for selecting which attribute to learn and which to remove
 */
public class CheckAttributesDetails {

    static org.apache.log4j.Logger LOGGER = Logging.getLogger(CheckAttributesDetails.class);

    public static void main(String args[]) {

        try {


            Options opt = new Options();

            opt.addOption("GHTraffic", false, "GHTraffic dataset");
            opt.addOption("Google", false, "Google dataset");
            opt.addOption("Twitter", false, "Twitter dataset");
            opt.addOption("Slack", false, "Slack dataset");

            CommandLineParser parser = new BasicParser();
            CommandLine cmd = parser.parse(opt, args);

            if (cmd.hasOption("Google")) {
                Instances instances = new ConverterUtils.DataSource("src/resources/google-all" + ".arff").getDataSet();

                for(int i=0;i<instances.numAttributes();i++) {

                    System.out.println("index:"+ i+", name:"+ instances.attribute(i).name()+", num of distinct values:"+ instances.numDistinctValues(i));
                }
            }

            if (cmd.hasOption("GHTraffic")) {
                Instances instances = new ConverterUtils.DataSource("src/resources/ghtraffic-all" + ".arff").getDataSet();

                for(int i=0;i<instances.numAttributes();i++) {

                    System.out.println("index:"+ i+", name:"+ instances.attribute(i).name()+", num of distinct values:"+ instances.numDistinctValues(i));
                }
            }

            if (cmd.hasOption("Twitter")) {
                Instances instances = new ConverterUtils.DataSource("src/resources/twitter-all" + ".arff").getDataSet();

                for(int i=0;i<instances.numAttributes();i++) {

                    System.out.println("index:"+ i+", name:"+ instances.attribute(i).name()+", num of distinct values:"+ instances.numDistinctValues(i));
                }
            }

            if (cmd.hasOption("Slack")) {
                Instances instances = new ConverterUtils.DataSource("src/resources/slack-all" + ".arff").getDataSet();

                for(int i=0;i<instances.numAttributes();i++) {

                    System.out.println("index:"+ i+", name:"+ instances.attribute(i).name()+", num of distinct values:"+ instances.numDistinctValues(i));
                }
            }



        }
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }
    }
}


