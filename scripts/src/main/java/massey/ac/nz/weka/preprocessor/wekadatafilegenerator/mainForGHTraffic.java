package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;


import massey.ac.nz.weka.Logging;

/**
 * main class to create csv file based on attribute-set selected in GHTraffic
 * and then to convert it to arff format which suitable for weka
 * @author thilini bhagya
 */
public class mainForGHTraffic {
    static org.apache.log4j.Logger LOGGER = Logging.getLogger(mainForGHTraffic.class);
    public static void main(String args[]) throws Exception {
        LOGGER.info("Generating a csv file relating with selected attributes");
        CsvFileGeneratorForGHTraffic.csvFileGeneratorWithAttributes("src/resources/ghtraffic-all","src/resources/outputGHTraffic");
        LOGGER.info("Generating an arff file from the csv");
        CsvToArffConverter.convertCsvDataFileToArffFormat("src/resources/ghtraffic-all");

    }
}
