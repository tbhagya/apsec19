package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import massey.ac.nz.weka.Logging;
import massey.ac.nz.weka.preprocessor.wekadatafilegenerator.*;


/**
 * main class to create csv file based on attribute-set selected in Google-Task-list
 * and then to convert it to arff format which suitable for weka
 * @author thilini bhagya
 */

public class mainForGoogleTaskLists {
    static org.apache.log4j.Logger LOGGER = Logging.getLogger(mainForGoogleTaskLists.class);
    public static void main(String args[]) throws Exception {
        LOGGER.info("Generating a csv file relating with selected attributes");
    CsvFileGeneratorForGoogleTaskLists.csvFileGeneratorWithAttributes("src/resources/google-all","src/resources/outputGoogle");
        LOGGER.info("Generating an arff file from the csv");
    CsvToArffConverter.convertCsvDataFileToArffFormat("src/resources/google-all");

    }
}
