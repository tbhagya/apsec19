package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import com.google.gson.*;
import com.google.gson.JsonParser;
import java.util.*;
import java.util.Set;

/**
 * This class tokenises each responseBody
 */
public class FeatureExtractor {
    public static void extractKeys(String section, String json2, Set<String> list){
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json2);
        JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject


        Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();//will return members of your object

        for (Map.Entry<String, JsonElement> entry: entries) {

            String keyStr = entry.getKey();
            Object keyvalue = obj.get(keyStr);

            if (keyvalue instanceof JsonObject) {
                JsonParser parsers = new JsonParser();
                JsonElement elements = parsers.parse(String.valueOf(keyvalue));
                JsonObject objs = elements.getAsJsonObject();

                Set<Map.Entry<String, JsonElement>> entriess = objs.entrySet();
                for (Map.Entry<String, JsonElement> entrie: entriess) {
                    list.add(section+":"+entry.getKey().concat(".").concat(entrie.getKey()));
            }
        }
            else {
                list.add(section+":"+entry.getKey());
            }
        }
    }

}
