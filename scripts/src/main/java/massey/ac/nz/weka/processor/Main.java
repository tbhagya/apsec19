package massey.ac.nz.weka.processor;

import massey.ac.nz.weka.Logging;
import org.apache.commons.cli.*;


/**
 * this is the main class which can be used to run
 * different experiments with different algorithms and datasets
 * @author thilinibhagya
 */

public class Main {

    static org.apache.log4j.Logger LOGGER = Logging.getLogger(Main.class);

    //create an Options for datasets

    public static final Option dataset1 = Option.builder("Google")
            .required(true)
            .longOpt("Google dataset")
            .build();

    public static final Option dataset2 = Option.builder("Slack")
            .required(true)
            .longOpt("Slack dataset")
            .build();

    public static final Option dataset3 = Option.builder("GHTraffic")
            .required(true)
            .longOpt("GHTraffic dataset")
            .build();

    public static final Option dataset4 = Option.builder("Twitter")
            .required(true)
            .longOpt("Twitter dataset")
            .build();

    //create an Options for algorithms
    public static final Option algo1 = Option.builder("J48")
            .required(true)
            .longOpt("J48 algorithm")
            .build();

    public static final Option algo2 = Option.builder("JRip")
            .required(true)
            .longOpt("JRip algorithm")
            .build();

    public static final Option algo3 = Option.builder("PART")
            .required(true)
            .longOpt("PART algorithm")
            .build();

    public static void main(String args[]) {
        //create an OptionGroup for datasets
        Options options = new Options();
        OptionGroup optgrp = new OptionGroup();
        optgrp.addOption(dataset1);
        optgrp.addOption(dataset2);
        optgrp.addOption(dataset3);
        optgrp.addOption(dataset4);

        options.addOptionGroup(optgrp);

        //create an OptionGroup for algorithms
        OptionGroup optgrp2 = new OptionGroup();
        optgrp2.addOption(algo1);
        optgrp2.addOption(algo2);
        optgrp2.addOption(algo3);

        options.addOptionGroup(optgrp2);


        try {

            CommandLineParser parser = new BasicParser();
            CommandLine cmd = parser.parse(options, args);

            //specify different options and related actions to be performed
            if (((cmd.hasOption("Google")) ) && ((cmd.hasOption("J48")) )) {
                J48Classifier.processWithJ48("src/resources/google-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }

            if (((cmd.hasOption("Slack")) ) && ((cmd.hasOption("J48")) )) {
                J48Classifier.processWithJ48("src/resources/slack-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }

            if (((cmd.hasOption("GHTraffic")) ) && ((cmd.hasOption("J48")) )) {
                J48Classifier.processWithJ48("src/resources/ghtraffic-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }

            if (((cmd.hasOption("Twitter")) ) && ((cmd.hasOption("J48")) )) {
                J48Classifier.processWithJ48("src/resources/twitter-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }

            if (((cmd.hasOption("Google")) ) && ((cmd.hasOption("JRip")) )) {
                JRipClassifier.processWithJRip("src/resources/google-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("Slack")) ) && ((cmd.hasOption("JRip")) )) {
                JRipClassifier.processWithJRip("src/resources/slack-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("GHTraffic")) ) && ((cmd.hasOption("JRip")) )) {
                JRipClassifier.processWithJRip("src/resources/ghtraffic-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("Twitter")) ) && ((cmd.hasOption("JRip")) )) {
                JRipClassifier.processWithJRip("src/resources/twitter-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }

            if (((cmd.hasOption("Google")) ) && ((cmd.hasOption("PART")) )) {
                PARTClassifier.processWithPART("src/resources/google-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("Slack")) ) && ((cmd.hasOption("PART")) )) {
                PARTClassifier.processWithPART("src/resources/slack-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("GHTraffic")) ) && ((cmd.hasOption("PART")) )) {
                PARTClassifier.processWithPART("src/resources/ghtraffic-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }
            if (((cmd.hasOption("Twitter")) ) && ((cmd.hasOption("PART")) )) {
                PARTClassifier.processWithPART("src/resources/twitter-all", ReadIndexesForInput.readIndexToLearn ("indexes.txt"),
                        ReadIndexesForInput.readIndexesToRemove ("indexes.txt"));
            }


        }
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }
    }
}
