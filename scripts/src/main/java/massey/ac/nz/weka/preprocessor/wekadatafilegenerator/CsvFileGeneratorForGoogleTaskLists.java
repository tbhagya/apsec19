package massey.ac.nz.weka.preprocessor.wekadatafilegenerator;

import com.opencsv.CSVWriter;
import org.json.JSONObject;
import com.google.gson.*;
import org.json.JSONArray;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.*;

/**
 * This class generates csv file for GoogleTaskLists dataset
 * related with attributes
 * @author thilini bhagya
 */
public class CsvFileGeneratorForGoogleTaskLists {

    static Set<String> responsebodykeylist = new TreeSet<String>();
    static Set<String> requestheaderskeylist = new TreeSet<String>();
    static Set<String> responseheaderskeylist = new TreeSet<String>();

    public static void csvFileGeneratorWithAttributes(String writeFileName, String readFileName) throws Exception {
        String csv = writeFileName + ".csv";
        CSVWriter writer = new CSVWriter(new FileWriter(csv), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        List<String[]> data = new ArrayList<String[]>();

        HttpTransaction.read(readFileName + ".csv");

        //extract key set for responseBody
        extractKeySetForResponseBody();
        //extract key set for headers
        //extractKeySetForRequestHeaders();
        //extractKeySetForResponseHeaders();

        //add the header line
        data.add(new String[]{"method","requestHeader:Connection","requestHeader:Content-Type","requestHeader:Host","requestHeader:User-Agent",
                "responseheader:Accept-Ranges","responseheader:Alt-Svc","responseheader:Cache-Control",
                "responseheader:Content-Length","responseheader:Content-Type","responseheader:Date",
                "responseheader:ETag","responseheader:Expires","responseheader:Pragma","responseheader:Server",
                "responseheader:Transfer-Encoding","responseheader:Vary","responseheader:X-Content-Type-Options","responseheader:X-Frame-Options",
                "responseheader:X-XSS-Protection",
                "statusCode",
                "json:error.code","json:error.errors.domain","json:error.errors.reason","json:error.errors.message","json:error.message","json:etag","json:id","json:kind","json:title","json:updated","json:selfLink",
                "hasAuthorizationToken",
                "uriSchema", "uriHost",
                "uriPathToken1", "uriPathToken2", "uriPathToken3", "uriPathToken4",
                "uriQueryToken1", "uriQueryToken2","uriQueryToken3",
                "uriFragmentToken1",
                "hasImmediatePreviousTransaction","immediatelyPreviousStatusCode", "immediatelyPreviousMethod",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToCreate", "hasURLInImmediatelyPreviousTransactionContainsATokenToRead",
                "hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate", "hasURLInImmediatelyPreviousTransactionContainsATokenToDelete",
                "has200StatusCodeOccurredPreviously", "has201StatusCodeOccurredPreviously", "has204StatusCodeOccurredPreviously",
                "has400StatusCodeOccurredPreviously", "has401StatusCodeOccurredPreviously", "has404StatusCodeOccurredPreviously",
                "has422StatusCodeOccurredPreviously", "has500StatusCodeOccurredPreviously", "has503StatusCodeOccurredPreviously",
                "hasAnyURLContainsATokenToCreate","hasAnyURLContainsATokenToRead", "hasAnyURLContainATokenToUpdate", "hasAnyURLContainATokenToDelete"

        });



        //responsebodykeylist.toString().replace("[","").replace("]","").replace(" ",""),

        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            List<String> method = new LinkedList<String>();
            List<String> code = new LinkedList<String>();
            List<String> action = new LinkedList<String>();

            for (List<HttpTransaction> mm : m.getValue().values()) {
                method.add(mm.get(0).getMethod());
                code.add(mm.get(0).getCode());
                action.add(mm.get(0).getURL());

                //Create record
                data.add(new String[]{
                        //method
                        String.valueOf(mm.get(0).getMethod()),
                        //request headers
                        String.valueOf(requestHeaders(mm,"Connection")),
                        String.valueOf(requestHeaders(mm,"Content-Type")),
                        String.valueOf(requestHeaders(mm,"Host")),
                        String.valueOf(requestHeaders(mm,"User-Agent")),
                        //String.valueOf(requestHeaders(mm,"Content-Length")),
                        //response headers
                        String.valueOf(responseHeaders(mm,"Accept-Ranges")),
                        String.valueOf(responseHeaders(mm,"Alt-Svc")),
                        String.valueOf(responseHeaders(mm,"Cache-Control")),
                        String.valueOf(responseHeaders(mm,"Content-Length")),
                        String.valueOf(responseHeaders(mm,"Content-Type")),
                        String.valueOf(responseHeaders(mm,"Date")),
                        String.valueOf(responseHeaders(mm,"ETag")),
                        String.valueOf(responseHeaders(mm,"Expires")),
                        String.valueOf(responseHeaders(mm,"Pragma")),
                        String.valueOf(responseHeaders(mm,"Server")),
                        String.valueOf(responseHeaders(mm,"Transfer-Encoding")),
                        String.valueOf(responseHeaders(mm,"Vary")),
                        String.valueOf(responseHeaders(mm,"X-Content-Type-Options")),
                        String.valueOf(responseHeaders(mm,"X-Frame-Options")),
                        String.valueOf(responseHeaders(mm,"X-XSS-Protection")),
                        //statusCode
                        String.valueOf(mm.get(0).getCode()),
                        //need to try by reading from the list other than adding names manually
                        //responseBody
                        //responseBody
                        String.valueOf(bodyinside(mm,"error","code")),
                        //responseBody
                        String.valueOf(bodyinsidesub(mm,"error","errors","domain")),
                        String.valueOf(bodyinsidesub(mm,"error","errors","reason")),
                        String.valueOf(bodyinsidesub(mm,"error","errors","message")),
                        //responseBody
                        String.valueOf(bodyinside(mm,"error","message")),
                        //responseBody
                        String.valueOf(body(mm,"etag")),
                        //responseBody
                        String.valueOf(body(mm,"id")),
                        //responseBody
                        String.valueOf(body(mm,"kind")),
                        //responseBody
                        String.valueOf(body(mm,"title")),
                        //responseBody
                        String.valueOf(body(mm,"updated")),
                        //responseBody
                        String.valueOf(body(mm,"selfLink")),
                        //hasAuthorizationToken
                        String.valueOf(hasAuthorizationToken(mm)),
                        //URLScheme
                        String.valueOf(UrlTokenizer.getURLScheme(mm.get(0).getURL())),
                        //URLHost
                        String.valueOf(UrlTokenizer.getUriHost(mm.get(0).getURL())),
                        //URLpathtokens
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken1")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken2")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken3")),
                        String.valueOf(UrlTokenizer.getURLCoreTokenMap(mm.get(0).getURL()).get("pathToken4")),
                        //URLquerytokens
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken1")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken2")),
                        String.valueOf(UrlTokenizer.getURLQueryTokenMap(mm.get(0).getURL()).get("queryToken3")),
                        //URLfragmenttokens
                        String.valueOf(UrlTokenizer.getFragmentMap(mm.get(0).getURL()).get("fragmentToken1")),
                        // hasImmediatePreviousTransaction
                        String.valueOf(hasImmediatePreviousTransaction(code)),
                        //immediatelyPreviousStatusCode
                        String.valueOf(immediatelyPreviousStatusCode(code)),
                        // immediatelyPreviousMethod
                        String.valueOf(immediatelyPreviousMethod(method)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToCreate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToRead
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToRead(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(code,action)),
                        //hasURLInImmediatelyPreviousTransactionContainsATokenToDelete
                        String.valueOf(hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(code,action)),
                        //has200StatusCodeOccurredPreviously
                        String.valueOf(has200StatusCodeOccurredPreviously(code)),
                        //has201OccurredPreviously
                        String.valueOf(has201StatusCodeOccurredPreviously(code)),
                        //has204OccurredPreviously
                        String.valueOf(has204StatusCodeOccurredPreviously(code)),
                        //has400OccurredPreviously
                        String.valueOf(has400StatusCodeOccurredPreviously(code)),
                        //has401OccurredPreviously
                        String.valueOf(has401StatusCodeOccurredPreviously(code)),
                        //has404OccurredPreviously
                        String.valueOf(has404StatusCodeOccurredPreviously(code)),
                        //has422OccurredPreviously
                        String.valueOf(has422StatusCodeOccurredPreviously(code)),
                        //has500OccurredPreviously
                        String.valueOf(has500StatusCodeOccurredPreviously(code)),
                        //has503StatusCodeOccurredPreviously
                        String.valueOf(has503StatusCodeOccurredPreviously(code)),
                        //hasAnyURLContainsATokenToCreate
                        String.valueOf(hasAnyURLContainsATokenToCreate(action)),
                        //hasAnyURLContainsATokenToRead
                        String.valueOf(hasAnyURLContainsATokenToRead(action)),
                        //hasAnyURLContainsATokenToUpdate
                        String.valueOf(hasAnyURLContainsATokenToUpdate(action)),
                        //hasAnyURLContainsATokenToDelete
                        String.valueOf(hasAnyURLContainsATokenToDelete(action))});
            }
        }

        //System.out.println(requestheaderskeylist);
        //System.out.println(responseheaderskeylist);
        //System.out.println(responsebodykeylist);
        // write out records
        writer.writeAll(data);
        //close the writer
        writer.close();
    }

    private static void extractKeySetForResponseHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                int index = mm.get(0).getResponseHeaders().indexOf("\t");
                String string_without_version = mm.get(0).getResponseHeaders().substring(index, mm.get(0).getResponseHeaders().length()).replaceAll("\"","");
                String stuff_with_curlyB = "{"+string_without_version+"}";
                String reg = stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
                String value = reg.replace("\"[\"{", "[{").replace("~", "\":\"").replace("}\"]\"", "}]").replace("\"true\"", "true").replace("\"false\"", "false");
                String values = value.replaceFirst("\t","");
                String a = values.replaceAll("\t",",");
                FeatureExtractor.extractKeys("responseheader", a,responseheaderskeylist);
                }
        }
    }

    private static void extractKeySetForRequestHeaders() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                String stuff_with_curlyB = "{"+mm.get(0).getRequestHeaders()+"}";
                String reg= stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
                String value=reg.replace("\"[\"{", "[{").replace("~","\":\"").replace("}\"]\"","}]").replace("\"true\"", "true").replace("\"false\"", "false");
                //System.out.println(value);
                String header=value.replaceAll("\t",",");
                FeatureExtractor.extractKeys("requestheader",value.replaceAll("\t",","),requestheaderskeylist);}
        }
    }

    private static void extractKeySetForResponseBody() {
        for (Map.Entry<String, TreeMap<String, List<HttpTransaction>>> m : HttpTransaction.transactions.entrySet()){
            for (List<HttpTransaction> mm : m.getValue().values()) {
                String string_without_version = mm.get(0).getResponseBody().replaceAll("\"\"","\"");

                if(string_without_version.contains("Non-TEXTresponsedata,cannotrecord")){


                }
                else {
                    FeatureExtractor.extractKeys("json",string_without_version,responsebodykeylist);
                    //System.out.println(string_without_version);
                }

                }
        }
    }


    public static boolean hasAuthorizationToken(List<HttpTransaction> mm) {
        boolean hasAuthorizationToken;
        if(String.valueOf(mm.get(0).getCode()).matches("401")){
            hasAuthorizationToken = false;
        }
        else{
            hasAuthorizationToken = true;
        }
        return hasAuthorizationToken;
    }

    public static String body(List<HttpTransaction> mm, String value) {
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody().replaceAll("\"\"","\"");

        if(string_without_version.contains("Non-TEXTresponsedata,cannotrecord")){


        }
        else {
            JSONObject jsonObject = new JSONObject(string_without_version);
            if (jsonObject.has(value)) {
                body = jsonObject.get(value).toString();
            }
        }

        return body;
    }

    public static String bodyinside(List<HttpTransaction> mm, String value, String subvalue) {
        String body = "no-exist";
        String string_without_version = mm.get(0).getResponseBody().replaceAll("\"\"","\"");
        if(string_without_version.contains("Non-TEXTresponsedata,cannotrecord")){


        }
        else {
            JSONObject jsonObject = new JSONObject(string_without_version);
            if (jsonObject.has(value)) {
                JSONObject jsonObject2 = new JSONObject(jsonObject.get(value).toString());
                if (jsonObject2.has(subvalue)) {
                    body = jsonObject2.get(subvalue).toString();

                }
            }
        }
        return body;
    }

   public static String bodyinsidesub(List<HttpTransaction> mm, String value, String subvalue,String subsubvalue) {
        String responseBody = "no-exist";
        String string_without_version = mm.get(0).getResponseBody().replaceAll("\"\"","\"");

        if(string_without_version.contains("Non-TEXTresponsedata,cannotrecord")){


        }
        else {
            //JSONObject jsonObject = new JSONObject(string_without_version);
            //if(jsonObject.has("error")){
               // System.out.println(jsonObject.get("error"));
               // JSONArray jArray1 = jsonObject.getJSONArray("errors");
               // JSONObject object3 = jArray1.getJSONObject(1);
               // System.out.println(object3);}

            JSONObject jsonObject2 = new JSONObject(string_without_version);
            //System.out.println(jsonObject2);

            if(jsonObject2.has(value)) {
                System.out.println(jsonObject2.getJSONObject("error").getJSONArray("errors"));

                JSONArray jArray1 = jsonObject2.getJSONObject(value).getJSONArray(subvalue);
                //System.out.println(jArray1.get(0));

                JsonParser parser = new JsonParser();
                JsonElement element = parser.parse(String.valueOf(jArray1.get(0)));
                JsonObject obj = element.getAsJsonObject();
                if(obj.has(subsubvalue)) {
                    responseBody = obj.get(subsubvalue).toString().replaceAll("\"","");
                }

            }

        }
        return responseBody;
    }

    public static String requestHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        //convert to json style
        String stuff_with_curlyB = "{"+mm.get(0).getRequestHeaders()+"}";
        String reg= stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
        String val=reg.replace("\"[\"{", "[{").replace("~","\":\"").replace("}\"]\"","}]").replace("\"true\"", "true").replace("\"false\"", "false");
        //System.out.println(value);
        String a=val.replaceAll("\t",",");
        JSONObject jsonObject = new JSONObject(a);
        if(jsonObject.has(value)){
            header = jsonObject.get(value).toString();
            //enclose entire field which contains commas to convert into .arff without any error

        }

        return header;
    }

    public static String responseHeaders(List<HttpTransaction> mm, String value) {
        String header = "no-exist";
        //convert to json style
        int index = mm.get(0).getResponseHeaders().indexOf("\t");
        String string_without_version = mm.get(0).getResponseHeaders().substring(index, mm.get(0).getResponseHeaders().length()).replaceAll("\"","");
        String stuff_with_curlyB = "{"+string_without_version+"}";
        String reg = stuff_with_curlyB.replaceAll("[^\\{\\}\t]+", "\"$0\"");
        String val = reg.replace("\"[\"{", "[{").replace("~", "\":\"").replace("}\"]\"", "}]").replace("\"true\"", "true").replace("\"false\"", "false");
        String values = val.replaceFirst("\t","");

        String a = values.replaceAll("\t",",");
        //System.out.println(a);
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(a);
        JsonObject obj = element.getAsJsonObject();
        if(obj.has(value)){
            header = (String.valueOf(obj.get(value)).replaceAll("\"",""));

            if(header.indexOf(",") >= 0){
                header = "\'"+ header +"\'";
            }
        }

        return header;
    }



    public static boolean hasImmediatePreviousTransaction(List<String> n) {
        boolean hasImmediatePreviousTransaction = false;
        if (n.subList(0,n.size()-1).size() != 0) {
            hasImmediatePreviousTransaction = true;
        }
        else {
            hasImmediatePreviousTransaction = false;
        }
        return hasImmediatePreviousTransaction;
    }

    public static String immediatelyPreviousStatusCode(List<String> code) {
        String immediatelyPreviousStatusCode = "no-exist";

        if(code.subList(0,code.size()-1).size()!=0){

            immediatelyPreviousStatusCode = code.subList(0,code.size()-1).get(code.subList(0,code.size()-1).size()-1);
        }
        else {
            immediatelyPreviousStatusCode = "no-exist";
        }

        return immediatelyPreviousStatusCode;
    }

    public static String immediatelyPreviousMethod(List<String> n) {
        String method = "no-exist";

        if(n.subList(0,n.size()-1).size()!=0){
            method = n.subList(0,n.size()-1).get(n.subList(0,n.size()-1).size()-1);
        }
        else {
            method = "no-exist";
        }

        return method;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToCreate(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("chat.postMessage")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToRead(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("show")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToUpdate(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("chat.update")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean hasURLInImmediatelyPreviousTransactionContainsATokenToDelete(List<String> code,List<String> action) {
        boolean hasImmediatePreviousTransaction = false;

        if(code.subList(0,code.size()-1).size()!=0){


            if(((action.subList(0, action.size() - 1).get(action.subList(0, action.size() - 1).size() - 1).contains("chat.delete")))) {
                hasImmediatePreviousTransaction = true;
            }
        }
        else {
            hasImmediatePreviousTransaction = false;
        }

        return hasImmediatePreviousTransaction;
    }

    public static boolean has200StatusCodeOccurredPreviously(List<String> code) {
        boolean has200StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("200") )) {

            has200StatusCodeOccurredPreviously = true;
        }
        else {
            has200StatusCodeOccurredPreviously = false;
        }
        return has200StatusCodeOccurredPreviously;
    }

    public static boolean has201StatusCodeOccurredPreviously(List<String> code) {
        boolean has201StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("201") )) {

            has201StatusCodeOccurredPreviously = true;
        }
        else {
            has201StatusCodeOccurredPreviously = false;
        }
        return has201StatusCodeOccurredPreviously;
    }

    public static boolean has204StatusCodeOccurredPreviously(List<String> code) {
        boolean has204StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("204") )) {

            has204StatusCodeOccurredPreviously = true;
        }
        else {
            has204StatusCodeOccurredPreviously = false;
        }
        return has204StatusCodeOccurredPreviously;
    }

    public static boolean has400StatusCodeOccurredPreviously(List<String> code) {
        boolean has400StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("400") )) {

            has400StatusCodeOccurredPreviously = true;
        }
        else {
            has400StatusCodeOccurredPreviously = false;
        }
        return has400StatusCodeOccurredPreviously;
    }

    public static boolean has401StatusCodeOccurredPreviously(List<String> code) {
        boolean has401StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("401") )) {

            has401StatusCodeOccurredPreviously = true;
        }
        else {
            has401StatusCodeOccurredPreviously = false;
        }
        return has401StatusCodeOccurredPreviously;
    }

    public static boolean has404StatusCodeOccurredPreviously(List<String> code) {
        boolean has404StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("404") )) {

            has404StatusCodeOccurredPreviously = true;
        }
        else {
            has404StatusCodeOccurredPreviously = false;
        }
        return has404StatusCodeOccurredPreviously;
    }

    public static boolean has422StatusCodeOccurredPreviously(List<String> code) {
        boolean has422StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("422") )) {

            has422StatusCodeOccurredPreviously = true;
        }
        else {
            has422StatusCodeOccurredPreviously = false;
        }
        return has422StatusCodeOccurredPreviously;
    }

    public static boolean has500StatusCodeOccurredPreviously(List<String> code) {
        boolean has500StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("500") )) {

            has500StatusCodeOccurredPreviously = true;
        }
        else {
            has500StatusCodeOccurredPreviously = false;
        }
        return has500StatusCodeOccurredPreviously;
    }

    public static boolean has503StatusCodeOccurredPreviously(List<String> code) {
        boolean has503StatusCodeOccurredPreviously = false;
        if (code.subList(0,code.size()-1).contains(("503") )) {

            has503StatusCodeOccurredPreviously = true;
        }
        else {
            has503StatusCodeOccurredPreviously = false;
        }
        return has503StatusCodeOccurredPreviously;
    }

    public static boolean hasAnyURLContainsATokenToCreate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "chat.postMessage"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToDelete(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "chat.delete"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToRead(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "show"));
        }
        return results.contains(true);
    }

    public static boolean hasAnyURLContainsATokenToUpdate(List<String> action) throws URISyntaxException {
        List Urls = action.subList(0,action.size()-1);
        List results = new LinkedList<String>();
        for (Object value : Urls) {
            results.add(UrlTokenizer.checkURLCoreTokenMapContainsValue((String) value, "chat.update"));
        }
        return results.contains(true);
    }

}
