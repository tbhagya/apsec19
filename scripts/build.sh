#!/bin/bash 
echo "Start project" 
mvn clean install
mvn -q clean compile exec:java -Dexec.mainClass="massey.ac.nz.weka.processor.Main" -Dexec.args="-$*"
exit 0